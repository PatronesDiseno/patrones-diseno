/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.List;

/**
 *
 * CRUD C(create)  R(read)  U(update) D(delete)
 */
public interface CRUD<T> {
    
    List<T> obtener();
    void agregar(T t);
    void editar(T t);
    void eliminar(int id);
    
    
}
