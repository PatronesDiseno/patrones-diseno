/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import entidades.Maestro;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lupita
 */
public class MaestroDAOImp implements MaestroDAO{

    @Override
    public List<Maestro> obtener() {
        return new ArrayList<Maestro>() ;
    }

    @Override
    public void agregar(Maestro m) {
        System.out.println("Se agrego correctamente el maestro "+m.getNombre());
    }

    @Override
    public void editar(Maestro m) {
        System.out.println("Se modifico correctamente el maestro "+m.getNombre());
    }

    @Override
    public void eliminar(int id) {
      System.out.println("Se elimino correctamente el maestro con id ="+id);
      
    }

    @Override
    public void impartirClase() {
        System.out.println("El maestro está impartiendo la clase");
    }
    
}
