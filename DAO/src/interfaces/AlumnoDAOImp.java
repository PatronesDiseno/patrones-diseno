/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import entidades.Estudiante;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lupita
 */
public class AlumnoDAOImp implements AlumnoDAO{

    @Override
    public List<Estudiante> obtener() {
   
     return new ArrayList<Estudiante>();
    }
    
    

    @Override
    public void agregar(Estudiante e) {
        System.out.println("Se agrego correctamente el alumno "+e.getNombre());

    }

    @Override
    public void editar(Estudiante e) {
      
        System.out.println("Se modifico correctamente el alumno "+e.getNombre());
    }

    @Override
    public void eliminar(int id) {
       
        System.out.println("Se elimino correctamente el alumno con id"+id);
    }

    @Override
    public void asistir() {
        System.out.println("El alumno se encuentra en clases");
    }
    
}
