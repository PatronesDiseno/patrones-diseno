/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import entidades.Estudiante;
import interfaces.AlumnoDAO;
import interfaces.AlumnoDAOImp;

/**
 *
 * @author lupita
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        AlumnoDAO alumno = new AlumnoDAOImp();
        alumno.agregar(new Estudiante(1,"Jose Pepe","jpepe@gmail.com"));
        alumno.asistir();
        
    }
    
}
