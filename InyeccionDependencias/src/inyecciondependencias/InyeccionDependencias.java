
package inyecciondependencias;

public class InyeccionDependencias {

    
    public static void main(String[] args) {
        
        //Al pasar los objetos inicializados se genera una DI(Dependency Injection)
        //En este caso se inyecta un servicio de envio con un agregado plus (Hereda del servicio de envio)
        ServicioImpresion servicioEjecutivo = new ServicioImpresion(new ServicioEnvioPlus(),
                new ServicioImpresionFormato());
        
        servicioEjecutivo.imprimir();
        
        System.out.println(" ===== == == == == = == == = ");
        
        //Al pasar los objetos inicializados se genera una DI(Dependency Injection)
        //En este caso se inyecta un servicio de envio sin mas.
        ServicioImpresion servicioEmpleado = new ServicioImpresion(new ServicioEnvio(),
                new ServicioImpresionFormato());
        servicioEmpleado.imprimir();
    }
    
}
