
package inyecciondependencias;


public class ServicioImpresion {
    
    ServicioImpresionFormato formato;
    ServicioEnvio envio;
 
    /**
     * El constructor acepta los objetos para inicializarlos
     * Está clase no los inicializa.
     * @param envio Objeto de servicio de envio (puede aceptar Herencias)
     * @param formato Objeto de Impresion de formato (puede aceptar Herencias)
     */
    public ServicioImpresion(ServicioEnvio envio, ServicioImpresionFormato formato) {
        this.formato = formato;
        this.envio = envio;
    }
    
    public void imprimir(){
        envio.enviar();
        formato.imprimir();
    }
    
}
