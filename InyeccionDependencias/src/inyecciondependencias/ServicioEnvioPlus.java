
package inyecciondependencias;

public class ServicioEnvioPlus  extends  ServicioEnvio{

    public ServicioEnvioPlus() {
    }

    @Override
    public void enviar() {
        System.out.println("Se ha añadido el servicio pluss");
        super.enviar();
    }
    
    
    
}
