package edu.itson;

import edu.itson.commands.Cuenta;
import edu.itson.commands.DepositarImpl;
import edu.itson.commands.Invoker;
import edu.itson.commands.RetirarImpl;

public class App {

	public static void main(String[] args) {
		Cuenta cuenta = new Cuenta(1, 200);

		DepositarImpl opDepositar = new DepositarImpl(cuenta, 100);
		RetirarImpl opRetirar = new RetirarImpl(cuenta, 50);
                RetirarImpl opRetirar2 = new RetirarImpl(cuenta, 150);

		Invoker ivk = new Invoker();
		ivk.recibirOperacion(opDepositar);
		ivk.recibirOperacion(opRetirar);
		ivk.recibirOperacion(opRetirar2);
		
		ivk.realizarOperaciones();
	}

}
