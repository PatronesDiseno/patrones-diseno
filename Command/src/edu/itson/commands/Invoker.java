package edu.itson.commands;

import java.util.ArrayList;
import java.util.List;

public class Invoker {

	private List<IOperacion> operaciones = new ArrayList<>();

	public void recibirOperacion(IOperacion operacion) {
		this.operaciones.add(operacion);
	}

	public void realizarOperaciones() {
		//Expresion Lambda
                this.operaciones.forEach(x -> x.execute());
                
//                for(IOperacion x:this.operaciones){
//                    x.execute();
//                }
                
		this.operaciones.clear();
	}

}
