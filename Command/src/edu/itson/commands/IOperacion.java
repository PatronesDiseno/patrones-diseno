package edu.itson.commands;

//Command
@FunctionalInterface
public interface IOperacion {

	void execute();
}
