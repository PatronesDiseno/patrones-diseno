/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import api.AvionAPI;
import api.HotelAPI;

/**
 *
 * @author lupita
 */
public class CheckFacade {
    
    private AvionAPI avionApi;
    private HotelAPI hotelApi;

    public CheckFacade() {
        avionApi = new AvionAPI();
        hotelApi = new HotelAPI();
        
    }
    
    public void buscar(String fechaIda, String fechaVuelta, String origen, String destino){
        avionApi.obtenerVuelos(fechaIda, fechaVuelta, origen, destino);
        hotelApi.obtenerHotel(fechaIda, fechaVuelta, origen, destino);
    }
    
    
    
}
