/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author jorge
 */
public class Prototype {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Ahorro juan = new Ahorro();
        juan.setNombre("Juan Alberto Mora Morales");
        juan.setAhorro(15000);
        juan.setUltimaFechaMov("12 Septiembre 2019");
        
        Ahorro juanCloned = (Ahorro) juan.clonar();
        
        Ahorro pedro = new Ahorro();
        
        System.out.println(juan);
        System.out.println(pedro);
        
        if(juanCloned != null){
            System.out.println(juanCloned);
        }
        
        System.out.println(juan == juanCloned);
        
    }
    
}
