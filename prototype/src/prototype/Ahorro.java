/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author jorge
 */
public class Ahorro implements IBanco{
    private String nombre;
    private double ahorro;
    private String ultimaFechaMov;

    public Ahorro() {
        nombre ="";
        ahorro =0.0;
        ultimaFechaMov = "01-Ene-1900";
    }   
    
    
    @Override
    public IBanco clonar() {
    
        Ahorro ahorroUser = null;
        
        try {
            ahorroUser = (Ahorro) clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ahorroUser;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getAhorro() {
        return ahorro;
    }

    public void setAhorro(double ahorro) {
        this.ahorro = ahorro;
    }

    public String getUltimaFechaMov() {
        return ultimaFechaMov;
    }

    public void setUltimaFechaMov(String ultimaFechaMov) {
        this.ultimaFechaMov = ultimaFechaMov;
    }

    @Override
    public String toString() {
        return "Ahorrador: "+nombre+"  - Ahorro: $"+ahorro+ "  - Ultimo movimiento: "+ultimaFechaMov; //To change body of generated methods, choose Tools | Templates.
    }

    
    
    
    
}
