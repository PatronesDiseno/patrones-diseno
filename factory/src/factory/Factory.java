/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

import interfaces.IFigura;

/**
 *
 * @author jorge
 */
public class Factory {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FiguraFactory factory = new FiguraFactory();
        
        IFigura circulo = factory.getFigura("circulo");
        IFigura cuadro = factory.getFigura("cuadrado");
        IFigura triangulo = factory.getFigura("triangulo");
        
        cuadro.dibujar();
        triangulo.dibujar();
        circulo.dibujar();
        
    }
    
}
