package edu.itson;

import edu.itson.inter.ICuenta;
import edu.itson.inter.impl.CuentaBancoAImpl;
import edu.itson.inter.impl.CuentaBancoBImpl;
import edu.itson.model.Cuenta;
import edu.itson.proxy.CuentaProxy;

public class App {

	public static void main(String[] args) {
		Cuenta c = new Cuenta(1, "Test Acc", 100);
		
		ICuenta cuentaProxy = new CuentaProxy(new CuentaBancoBImpl());
		cuentaProxy.mostrarSaldo(c);
		c = cuentaProxy.depositarDinero(c, 50);
		c = cuentaProxy.retirarDinero(c, 20);
		cuentaProxy.mostrarSaldo(c);
		
	}

}
