/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

import model.Paises;

/**
 *
 * @author jorge
 */
public class Singleton {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Paises p1 = Paises.getInstance();        
//        Paises p2 = new Paises();
        Paises p2 = Paises.getInstance();
        
        p1.mostrarPaises();
        p2.mostrarPaises();
        
        p1.nuevoNombre("Africa");
        
        
        p1.mostrarPaises();
        p2.mostrarPaises();
        
        boolean res = p1 instanceof Paises;
        boolean res2 = p2 instanceof Paises;
        
        System.out.println("res "+res);
        System.out.println("res2 "+res2);
        
    }
    
}
