/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author jorge
 */
public class Paises {
    private static Paises instancia;
    private String nombres;
    
    private Paises(){
     nombres = "Mexico";
    }
    
    public static Paises getInstance(){
        if(instancia == null){
            instancia = new Paises();           
        }
        return instancia;
    }
    
    public void nuevoNombre(String nombre){
        this.nombres = nombre;
    }
    
    public void mostrarPaises(){
        System.out.println("Hola a todos los países del mundo desde "+nombres);
    }
    
}
