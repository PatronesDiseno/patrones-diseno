/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.language;

import abstractfactory.interfaces.IApplication;

/**
 *
 * @author jorge
 */
public class Java implements IApplication{

    @Override
    public void desktop() {
        System.out.println("Se desarrolla una app Java");
    }

    @Override
    public void web() {
        System.out.println("Se desarrolla una app Java Server Faces");
    }

    @Override
    public void mobile() {
        System.out.println("Se desarrolla una app Android");
    }
    
}
