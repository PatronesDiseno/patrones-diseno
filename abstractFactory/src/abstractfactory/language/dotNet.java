/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.language;

import abstractfactory.interfaces.IApplication;

/**
 *
 * @author jorge
 */
public class dotNet implements IApplication{

    @Override
    public void desktop() {
        System.out.println("Se desarrolla una app Windows.");
    }

    @Override
    public void web() {
        System.out.println("Se desarrolla una app ASP");
    }

    @Override
    public void mobile() {
        System.out.println("Se desarrolla una app Xamarin");
    }
    
}
